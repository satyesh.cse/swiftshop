package app.swiftshop.entities;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name="INVENTORY_INFO")
public class InventoryInfo {
	
	@Id
	private String itemcode;
	private String itemName;
	private Integer stockLevel;
	private Float itemPrice;
	private String itemType;
	private Float itemSellPrice;
	private LocalDate useByDate;
	
	public String getItemcode() {
		return itemcode;
	}
	public void setItemcode(String itemcode) {
		this.itemcode = itemcode;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public Integer getStockLevel() {
		return stockLevel;
	}
	public void setStockLevel(Integer stockLevel) {
		this.stockLevel = stockLevel;
	}
	public Float getItemPrice() {
		return itemPrice;
	}
	public String getItemType() {
		return itemType;
	}
	public void setItemType(String itemType) {
		this.itemType = itemType;
	}
	public Float getItemSellPrice() {
		return itemSellPrice;
	}
	public void setItemSellPrice(Float itemSellPrice) {
		this.itemSellPrice = itemSellPrice;
	}
	public void setItemPrice(Float itemPrice) {
		this.itemPrice = itemPrice;
	}
	public LocalDate getUseByDate() {
		return useByDate;
	}
	public void setUseByDate(LocalDate useByDate) {
		this.useByDate = useByDate;
	}
	@Override
	public String toString() {
		return "InventoryInfo [itemcode=" + itemcode + ", itemName=" + itemName + ", stockLevel=" + stockLevel
				+ ", itemPrice=" + itemPrice + ", itemType=" + itemType + ", itemSellPrice=" + itemSellPrice + "]";
	}
	

}
