package app.swiftshop.controller.advice;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import app.swiftshop.exception.BadRequestException;
import app.swiftshop.exception.NotFoundException;

@ControllerAdvice
public class InventoryControllerAdvice extends ResponseEntityExceptionHandler{
	
	@ExceptionHandler(value=BadRequestException.class)
	public ResponseEntity<String> badreq()
	{
		return ResponseEntity.badRequest().body("Bad input");
	}

	@ExceptionHandler(value=NotFoundException.class)
	public ResponseEntity<String> notfoundexp()
	{
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Not Found");
	}
}
