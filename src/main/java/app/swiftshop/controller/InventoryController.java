package app.swiftshop.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import app.swiftshop.entities.InventoryInfo;
import app.swiftshop.service.InventoryService;

@RestController
@RequestMapping("/inventory")
public class InventoryController {
	
	@Autowired
	private InventoryService inventoryService;
	
	private Logger logger=LoggerFactory.getLogger(InventoryController.class);

	@PostMapping("/stock")
	public ResponseEntity<String> postStock(@RequestBody InventoryInfo record)
	{
		logger.info("got request {}",record);
		inventoryService.createStock(record);
		return ResponseEntity.created(null).build();	
	}
	
	@GetMapping("/stock")
	public ResponseEntity<List<InventoryInfo>> getStock()
	{
		List<InventoryInfo> stocks=null;
		
			stocks=inventoryService.getstock();
		return ResponseEntity.ok(stocks);
	}
	
	@GetMapping("/stock/type{itemtype}")
	public ResponseEntity<List<InventoryInfo>> getStock(@PathVariable("itemtype") String itemtype)
	{
		List<InventoryInfo> stocks=null;
		
			stocks=inventoryService.getstockByType(itemtype);
		return ResponseEntity.ok(stocks);
	}
	
	@GetMapping("/stock/{itemCode}")
	public ResponseEntity<InventoryInfo> getStockByItem(@PathVariable("itemCode") String itemCode)
	{
		InventoryInfo stocks=inventoryService.getstock(itemCode);
		return ResponseEntity.ok(stocks);
	}
	
	
	@PutMapping("/stock/{itemCode}")
	public ResponseEntity<String> modifyStock(@PathVariable("itemCode") String itemCode,@RequestBody InventoryInfo record)
	{
		String result=inventoryService.modifyStock(itemCode,record);
		return ResponseEntity.ok(result);
	}
}
