package app.swiftshop.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.jboss.logging.MDC;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Aspect
@Configuration
public class LogHandlerAspect {

	private static final Logger log = LoggerFactory.getLogger(LogHandlerAspect.class);

	@Pointcut("execution(* app.swiftshop.controller.*.*(..))")
	public void controllermethods() {
	}

	@Pointcut("execution(* app.swiftshop.controller.*.*(..))")
	public void controllerException() {
	}

	@Before("controllermethods()")
	public void requestLogPointCut(JoinPoint jp) {
		Object[] args = jp.getArgs();
		for (Object o : args) {
			log.info(" TraceId {} Request Object :{}",MDC.get("TraceId"), o);
		}
	}

	@AfterReturning(value = "controllermethods()", returning = "r")
	public void requestRespPointCut(JoinPoint jp, ResponseEntity<?> r) {

		if (r != null && r.hasBody()) {
			ObjectMapper oj = new ObjectMapper();
			try {
				String resp = oj.writeValueAsString(r.getBody());
				log.info(" TraceId {} Response Object :{}",MDC.get("TraceId"),resp);
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}

		}
	}
}
