package app.swiftshop.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import app.swiftshop.entities.InventoryInfo;


public interface InventoryRepository  extends JpaRepository<InventoryInfo, String>{

	List<InventoryInfo> findByItemType(String type);

}
