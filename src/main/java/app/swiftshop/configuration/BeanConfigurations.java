package app.swiftshop.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import app.swiftshop.controller.filter.TraceFilter;

@Configuration
public class BeanConfigurations {

	@Autowired
	ApplicationContext ac;
	
	public void getInventoryValidator()
	{
		//return ac.getBean(InventoryValidator.class);
	}
	
	@Bean
	public FilterRegistrationBean<TraceFilter> loggingFilter(){
	    FilterRegistrationBean<TraceFilter> registrationBean 
	      = new FilterRegistrationBean<>();
	         
	    registrationBean.setFilter(new TraceFilter());
	    registrationBean.addUrlPatterns("/*");
	         
	    return registrationBean;    
	}
}
