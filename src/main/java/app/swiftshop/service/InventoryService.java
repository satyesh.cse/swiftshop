package app.swiftshop.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.swiftshop.entities.InventoryInfo;
import app.swiftshop.exception.ItemExistsException;
import app.swiftshop.exception.NotFoundException;
import app.swiftshop.repository.InventoryRepository;
import app.swiftshop.validator.InventoryValidator;;

@Service
public class InventoryService {

	private InventoryRepository inventoryRepository;
	private InventoryValidator inventoryValidator;

	@Autowired
	InventoryService(InventoryRepository inventoryRepository,InventoryValidator inventoryValidator) {
		this.inventoryRepository = inventoryRepository;
		this.inventoryValidator=inventoryValidator;
	}

	public void createStock(InventoryInfo record) {
		
		inventoryRepository.save(record);
	}

	public List<InventoryInfo> getstock() {
		return inventoryRepository.findAll();
	}

	public List<InventoryInfo> getstockByType(String type) {
		return inventoryRepository.findByItemType(type);
	}
	

	public InventoryInfo getstock(String itemCode) {
		
		return inventoryRepository.findById(itemCode).orElseThrow(() -> new NotFoundException("item not found"));
	}

	public String modifyStock(String itemCode, InventoryInfo record) {
		inventoryValidator.validateItemExists(itemCode);
		if(!itemCode.equals(record.getItemcode()))
		{
			throw new NotFoundException("Item mismatch");
		}
		inventoryRepository.save(record);
		
		return "applied";
	}
	
	private void validateItemExists(InventoryInfo record)
	{
		if (inventoryValidator.validateItemExists(record.getItemcode())) {
			throw new ItemExistsException();
		}
	}
}
