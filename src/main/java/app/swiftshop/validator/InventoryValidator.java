package app.swiftshop.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import app.swiftshop.entities.InventoryInfo;
import app.swiftshop.exception.BadRequestException;
import app.swiftshop.repository.InventoryRepository;


@Service
public class InventoryValidator {

@Autowired
private InventoryRepository inventoryRepository;

public InventoryValidator(InventoryRepository inventoryRepository) {
	super();
	this.inventoryRepository = inventoryRepository;
}

public boolean validateItemExists(String itemCode) {
	boolean output = false;
	if(itemCode==null)
	{
		return output;
	}
	
	if (inventoryRepository.findById(itemCode).isPresent()) {
		output = true;
	}
	return output;
}

public void validateInputs(InventoryInfo record)
{
	if(StringUtils.isEmpty(record))
	{
		
	}
}

public void validateOperation(String operation) {
	if(operation==null)
	{
		throw new BadRequestException("operation can not be empty");
	}
	if(!operation.equals("ADD") && !operation.equals("SUB"))
	{
		throw new BadRequestException("operation not supported");
	}
}




	
}
